package ru.t1.azarin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.project.ProjectShowByIdRequest;
import ru.t1.azarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.azarin.tm.dto.model.ProjectDTO;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "project-show-by-id";

    @NotNull
    public final static String DESCRIPTION = "Show project by id.";

    @Override
    public void execute() {
        System.out.println("[FIND PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);
        @NotNull final ProjectDTO project = getProjectEndpoint().showByIdResponse(request).getProject();
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
