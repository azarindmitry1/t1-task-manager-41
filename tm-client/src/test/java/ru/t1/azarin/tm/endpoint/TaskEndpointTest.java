package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.azarin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.project.ProjectClearRequest;
import ru.t1.azarin.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.azarin.tm.dto.request.task.*;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.azarin.tm.dto.response.task.*;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.dto.model.ProjectDTO;
import ru.t1.azarin.tm.dto.model.TaskDTO;
import ru.t1.azarin.tm.service.PropertyService;

import java.sql.SQLException;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(PROPERTY_SERVICE);

    @Nullable
    private static String ADMIN_TOKEN;

    @NotNull
    private static final String ADMIN_LOGIN = "admin";

    @NotNull
    private static final String ADMIN_PASSWORD = "admin";

    @NotNull
    private final String testString = "TEST_STRING";

    @NotNull
    private final String testString2 = "TEST_STRING2";

    @NotNull
    private final String testString3 = "TEST_STRING3";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @Nullable
    private ProjectDTO adminProject1;

    @Nullable
    private TaskDTO adminTask1;

    @Nullable
    private TaskDTO adminTask2;

    @BeforeClass
    public static void setUp() throws SQLException {
        ADMIN_TOKEN = AUTH_ENDPOINT.login(new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD)).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(request);
    }

    @Before
    public void before() {
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(ADMIN_TOKEN);
        createRequest.setName(testString);
        createRequest.setDescription(testString);
        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createResponse(createRequest);
        adminProject1 = createResponse.getProject();

        @NotNull final TaskCreateRequest createRequest1 = new TaskCreateRequest(ADMIN_TOKEN);
        createRequest1.setName(testString);
        createRequest1.setDescription(testString);
        @NotNull final TaskCreateResponse createResponse1 = taskEndpoint.createTaskResponse(createRequest1);
        adminTask1 = createResponse1.getTask();

        @NotNull final TaskCreateRequest createRequest2 = new TaskCreateRequest(ADMIN_TOKEN);
        createRequest2.setName(testString2);
        createRequest2.setDescription(testString2);
        @NotNull final TaskCreateResponse createResponse2 = taskEndpoint.createTaskResponse(createRequest2);
        adminTask2 = createResponse2.getTask();
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(ADMIN_TOKEN);
        projectEndpoint.clearResponse(clearRequest);

        @NotNull final TaskClearRequest clearRequest1 = new TaskClearRequest(ADMIN_TOKEN);
        taskEndpoint.clearTaskResponse(clearRequest1);
    }

    @Test
    public void changeTaskStatusByIdResponse() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIdResponse(new TaskChangeStatusByIdRequest(nullString))
        );
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(ADMIN_TOKEN);
        request.setId(adminTask1.getId());
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void clearTask() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTaskResponse(new TaskClearRequest(nullString))
        );
        @NotNull final TaskClearRequest request = new TaskClearRequest(ADMIN_TOKEN);
        taskEndpoint.clearTaskResponse(request);
        Assert.assertNull(taskEndpoint.listTaskResponse(new TaskListRequest(ADMIN_TOKEN)).getTasks());
    }

    @Test
    public void completeTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.completeTaskByIdResponse(new TaskCompleteByIdRequest(nullString))
        );
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(ADMIN_TOKEN);
        request.setId(adminTask1.getId());
        @Nullable final TaskCompleteByIdResponse response = taskEndpoint.completeTaskByIdResponse(request);
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTaskResponse(new TaskCreateRequest(nullString))
        );
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(ADMIN_TOKEN);
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final TaskCreateResponse response = taskEndpoint.createTaskResponse(request);
        Assert.assertEquals(testString3, response.getTask().getName());
        Assert.assertEquals(testString3, response.getTask().getDescription());
    }

    @Test
    public void listTaskByProjectId() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskByProjectIdResponse(new TaskListByProjectIdRequest(nullString))
        );
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(ADMIN_TOKEN);
        bindRequest.setProjectId(adminProject1.getId());
        bindRequest.setTaskId(adminTask1.getId());
        taskEndpoint.bindTaskToProjectResponse(bindRequest);
        @NotNull final TaskListByProjectIdRequest listRequest = new TaskListByProjectIdRequest(ADMIN_TOKEN);
        listRequest.setId(adminProject1.getId());
        @Nullable final TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectIdResponse(listRequest);
        Assert.assertEquals(1, response.getTasks().size());
    }

    @Test
    public void list() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTaskResponse(new TaskListRequest(nullString))
        );
        @NotNull final TaskListRequest request = new TaskListRequest(ADMIN_TOKEN);
        @Nullable final TaskListResponse response = taskEndpoint.listTaskResponse(request);
        Assert.assertEquals(2, response.getTasks().size());
    }

    @Test
    public void removeTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskByIdResponse(new TaskRemoveByIdRequest(nullString))
        );
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(ADMIN_TOKEN);
        request.setId(adminTask1.getId());
        @Nullable final TaskRemoveByIdResponse response = taskEndpoint.removeTaskByIdResponse(request);
        Assert.assertEquals(adminTask1.getId(), response.getTask().getId());
    }

    @Test
    public void showTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.showTaskByIdResponse(new TaskShowByIdRequest(nullString))
        );
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(ADMIN_TOKEN);
        request.setId(adminTask1.getId());
        @Nullable final TaskShowByIdResponse response = taskEndpoint.showTaskByIdResponse(request);
        Assert.assertEquals(adminTask1.getName(), response.getTask().getName());
        Assert.assertEquals(adminTask1.getDescription(), response.getTask().getDescription());
    }

    @Test
    public void startTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.startTaskByIdResponse(new TaskStartByIdRequest(nullString))
        );
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(ADMIN_TOKEN);
        request.setId(adminTask1.getId());
        @Nullable final TaskStartByIdResponse response = taskEndpoint.startTaskByIdResponse(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProjectResponse(new TaskBindToProjectRequest(nullString))
        );
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(ADMIN_TOKEN);
        request.setProjectId(adminProject1.getId());
        request.setTaskId(adminTask1.getId());
        @Nullable final TaskBindToProjectResponse response = taskEndpoint.bindTaskToProjectResponse(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProjectResponse(new TaskUnbindFromProjectRequest(nullString))
        );
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(ADMIN_TOKEN);
        request.setProjectId(adminProject1.getId());
        request.setTaskId(adminTask1.getId());
        taskEndpoint.unbindTaskFromProjectResponse(request);
        Assert.assertEquals(null, adminTask1.getProjectId());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIdResponse(new TaskUpdateByIdRequest(nullString))
        );
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(ADMIN_TOKEN);
        request.setId(adminTask1.getId());
        request.setName(testString3);
        request.setDescription(testString3);
        @Nullable final TaskUpdateByIdResponse response = taskEndpoint.updateTaskByIdResponse(request);
        Assert.assertEquals(testString3, response.getTask().getName());
        Assert.assertEquals(testString3, response.getTask().getDescription());
    }

}
