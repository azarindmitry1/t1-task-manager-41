package ru.t1.azarin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, user_id, project_id, name, description, status, created)" +
            " VALUES (#{id}, #{userId}, #{projectId}, #{name}, #{description}, #{status}, #{created})")
    void add(@NotNull TaskDTO task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} and id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    TaskDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} and id = #{id}")
    void remove(@NotNull TaskDTO task);

    @Update("UPDATE tm_task SET name = #{name}, description = #{description}, status = #{status}, " +
            "project_id = #{projectId} WHERE id = #{id}")
    void update(@NotNull TaskDTO task);

}