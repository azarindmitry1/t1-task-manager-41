package ru.t1.azarin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, user_id, date, role) " +
            "VALUES (#{id}, #{userId}, #{date}, #{role})")
    void add(@NotNull SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<SessionDTO> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} and id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    SessionDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} and id = #{id}")
    void remove(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

}
