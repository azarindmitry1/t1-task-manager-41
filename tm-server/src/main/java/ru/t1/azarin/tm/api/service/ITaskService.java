package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO add(@Nullable TaskDTO task);

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable TaskDTO task);

    void removeById(@Nullable String userId, @Nullable String id);

    void update(@Nullable TaskDTO task);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}