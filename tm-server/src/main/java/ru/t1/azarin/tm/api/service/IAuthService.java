package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.SessionDTO;
import ru.t1.azarin.tm.dto.model.UserDTO;

import java.sql.SQLException;

public interface IAuthService {

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    UserDTO registry(@NotNull String login, @NotNull String password, @NotNull String email) throws SQLException;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws SQLException;

}
