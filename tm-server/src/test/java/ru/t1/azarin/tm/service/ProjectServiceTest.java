package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.service.*;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.DescriptionEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.NameEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.dto.model.ProjectDTO;
import ru.t1.azarin.tm.dto.model.UserDTO;
import ru.t1.azarin.tm.util.HashUtil;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT2;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectService PROJECT_SERVICE = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(
            PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE
    );

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "TEST_STRING";

    @BeforeClass
    public static void setUp() {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        USER_SERVICE.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER1_LOGIN);
        USER_SERVICE.remove(user);
    }

    @Before
    public void before() {
        USER1_PROJECT1.setUserId(USER_ID);
        USER1_PROJECT2.setUserId(USER_ID);
        PROJECT_SERVICE.add(USER1_PROJECT1);
        PROJECT_SERVICE.add(USER1_PROJECT2);
    }

    @After
    public void after() {
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Test
    public void add() {
        Assert.assertThrows(EntityNotFoundException.class, () -> PROJECT_SERVICE.add(null));
        PROJECT_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, PROJECT_SERVICE.findAll(USER_ID).size());
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(testString);
        project.setDescription(testString);
        project.setUserId(USER_ID);
        PROJECT_SERVICE.add(project);
        Assert.assertEquals(1, PROJECT_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void create() {
        PROJECT_SERVICE.remove(USER1_PROJECT1);
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(
                emptyString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(
                nullString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(
                USER_ID, emptyString, USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(
                USER_ID, nullString, USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.create(
                USER_ID, USER1_PROJECT1.getName(), emptyString)
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.create(
                USER_ID, USER1_PROJECT1.getName(), nullString)
        );
        @Nullable final ProjectDTO project = PROJECT_SERVICE.create(
                USER_ID, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription()
        );
        Assert.assertEquals(USER1_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER1_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER1_PROJECT1.getUserId(), project.getUserId());
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.clear(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.clear(nullString));
        PROJECT_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, PROJECT_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(
                emptyString, USER1_PROJECT1.getId(), Status.COMPLETED)
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(
                nullString, USER1_PROJECT1.getId(), Status.COMPLETED)
        );
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(
                USER_ID, emptyString, Status.COMPLETED)
        );
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(
                USER_ID, nullString, Status.COMPLETED)
        );
        PROJECT_SERVICE.changeProjectStatusById(USER_ID, USER1_PROJECT1.getId(), Status.COMPLETED);
        @Nullable final ProjectDTO project = PROJECT_SERVICE.findOneById(USER_ID, USER1_PROJECT1.getId());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void existById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.existById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.existById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.existById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.existById(USER_ID, nullString));
        Assert.assertTrue(PROJECT_SERVICE.existById(USER_ID, USER1_PROJECT1.getId()));
        Assert.assertFalse(PROJECT_SERVICE.existById(USER_UNREGISTRY_ID, USER1_PROJECT1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll(nullString));
        Assert.assertEquals(0, PROJECT_SERVICE.findAll(USER_UNREGISTRY_ID).size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findOneById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findOneById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.findOneById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.findOneById(USER_ID, nullString));
        @Nullable final ProjectDTO project = PROJECT_SERVICE.findOneById(USER_ID, USER1_PROJECT1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), project.getId());
    }

    @Test
    public void remove() {
        Assert.assertThrows(EntityNotFoundException.class, () -> PROJECT_SERVICE.remove(null));
        PROJECT_SERVICE.remove(USER1_PROJECT1);
        Assert.assertEquals(1, PROJECT_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeById(emptyString, USER1_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeById(nullString, USER1_PROJECT1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.findOneById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.findOneById(USER_ID, nullString));
        PROJECT_SERVICE.removeById(USER_ID, USER1_PROJECT1.getId());
        Assert.assertEquals(1, PROJECT_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateById(
                emptyString, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateById(
                nullString, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.updateById(
                USER_ID, emptyString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.updateById(
                USER_ID, nullString, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.updateById(
                USER_ID, USER1_PROJECT1.getId(), emptyString, USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.updateById(
                USER_ID, USER1_PROJECT1.getId(), nullString, USER1_PROJECT1.getDescription())
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.updateById(
                USER_ID, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), emptyString)
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.updateById(
                USER_ID, USER1_PROJECT1.getId(), USER1_PROJECT1.getName(), nullString)
        );
        PROJECT_SERVICE.updateById(USER_ID, USER1_PROJECT1.getId(), testString, testString);
        @Nullable final ProjectDTO project = PROJECT_SERVICE.findOneById(USER_ID, USER1_PROJECT1.getId());
        Assert.assertEquals(testString, project.getName());
    }

}
